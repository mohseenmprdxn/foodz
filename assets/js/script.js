// jQuery function inside 
$(document).ready(function(){
    // tab functionality for food section 
    var links = $(".tab-buttons li h3 a")
    var food = $(".tab-section ul"); 
    $(links).click(function (e) {
      e.preventDefault();
      $(links).removeClass("active"); // removing class from the buton
      $(this).addClass("active"); // adding class on click
      $(food).removeClass("active"); // removing class from the food section on click
      $(food).eq(links.index(this)).addClass("active"); // adding active class on the click of a button
    });

    // responsive hamburger functionality added
    var hamburger = $('.ham')
    var mobNav = $('nav ul')
    $(hamburger).click(function () {
        $(this).toggleClass("open");
        $(mobNav).toggle();
    })         
});